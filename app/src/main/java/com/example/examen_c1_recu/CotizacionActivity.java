package com.example.examen_c1_recu;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

public class CotizacionActivity extends AppCompatActivity {
    // Declarar variables
    private TextView lblNombreCliente;
    private TextView lblFolio;

    private EditText txtDescripcion;
    private EditText txtValorAuto;
    private EditText txtPagoInicial;

    private RadioButton rbPlazo12Meses;
    private RadioButton rbPlazo18Meses;
    private RadioButton rbPlazo24Meses;
    private RadioButton rbPlazo36Meses;

    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;

    private TextView lblPagoMensual;
    private TextView lblEnganche;

    private Cotización cotizacion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cotizacion);
        iniciarComponentes();
        Bundle datos = getIntent().getExtras();
        String nombre = datos.getString("nombre");
        lblNombreCliente.setText(nombre);
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcular();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar(v);
            }
        });

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcular();
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                regresar(v);
            }
        });
    }

    private void iniciarComponentes() {
        lblNombreCliente = (TextView) findViewById(R.id.lblNombreCliente2);
        lblFolio = (TextView) findViewById(R.id.lblFolio);
        lblPagoMensual = (TextView) findViewById(R.id.lblPagoMensual);
        lblEnganche = (TextView) findViewById(R.id.lblEnganche);

        txtDescripcion = (EditText) findViewById(R.id.txtDescripcion);
        txtValorAuto = (EditText) findViewById(R.id.txtValorAuto);
        txtPagoInicial = (EditText) findViewById(R.id.txtPagoInicial);

        rbPlazo12Meses = (RadioButton) findViewById(R.id.rbPlazo12Meses);
        rbPlazo18Meses = (RadioButton) findViewById(R.id.rbPlazo18Meses);
        rbPlazo24Meses = (RadioButton) findViewById(R.id.rbPlazo24Meses);
        rbPlazo36Meses = (RadioButton) findViewById(R.id.rbPlazo36Meses);

        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);

        cotizacion = new Cotización(0, "", 0, 0, 0);
        lblFolio.setText(""+cotizacion.generarFolio());
        rbPlazo12Meses.setChecked(true);



    }

    private void calcular() {
        String descripcion = txtDescripcion.getText().toString();
        String valorAutoStr = txtValorAuto.getText().toString();
        String pagoInicialStr = txtPagoInicial.getText().toString();

        if (descripcion.isEmpty()) {
            txtDescripcion.setError("Ingrese una descripción");
            return;
        }

        if (valorAutoStr.isEmpty()) {
            txtValorAuto.setError("Ingrese un valor válido");
            return;
        }

        if (pagoInicialStr.isEmpty()) {
            txtPagoInicial.setError("Ingrese un valor válido");
            return;
        }

        float valorAuto = Float.parseFloat(valorAutoStr);
        float pagoInicial = Float.parseFloat(pagoInicialStr);
        int plazo = 1;

        if (rbPlazo12Meses.isChecked()) {
            plazo = 12;
        } else if (rbPlazo18Meses.isChecked()) {
            plazo = 18;
        } else if (rbPlazo24Meses.isChecked()){
            plazo = 24;
        } else if(rbPlazo36Meses.isChecked()) {
            plazo = 36;
        }

        cotizacion.setDescripcion(descripcion);
        cotizacion.setValorAuto(valorAuto);
        cotizacion.setPorEnganche(pagoInicial);
        cotizacion.setPlazo(plazo);

        float enganche = cotizacion.calcularEnganche(valorAuto, pagoInicial);
        float pagoMensual = cotizacion.calcularPagoMensual(valorAuto, pagoInicial, plazo);

        lblPagoMensual.setText("Pago Mensual en: " + pagoMensual);
        lblEnganche.setText("Enganche: " + enganche);
    }
    public void limpiar(View v) {

        lblNombreCliente = findViewById(R.id.lblNombreCliente2);
        txtDescripcion.setText("");
        txtValorAuto.setText("");
        txtPagoInicial.setText("");

        lblPagoMensual.setText("");
        lblEnganche.setText("");
    }

    public void regresar(View v) {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Cotizacion APP");
        confirmar.setMessage("Regresar a la pantalla pricipal?");
        confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        confirmar.show();
    }

    // Cotizacion Acyivity Terminado

}

