package com.example.examen_c1_recu;

public class Cotización {
    // Declarar de variables
    private int folio;
    private String descripcion;
    private float valorAuto;
    private float porEnganche;
    private int plazo;

    public Cotización(int folio, String descripcion, float valorAuto, float porEnganche, int plazo) {
        this.folio = folio;
        this.descripcion = descripcion;
        this.valorAuto = valorAuto;
        this.porEnganche = porEnganche;
        this.plazo = plazo;
    }

    public int getFolio() {
        return folio;
    }

    public void setFolio(int folio) {
        this.folio = folio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public float getPorEnganche() {
        return porEnganche;
    }

    public void setPorEnganche(float porEnganche) {
        this.porEnganche = porEnganche;
    }

    public float getValorAuto() {
        return valorAuto;
    }

    public void setValorAuto(float valorAuto) {
        this.valorAuto = valorAuto;
    }

    public int getPlazo() {
        return plazo;
    }

    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }

    public int generarFolio() {
        return 0123;
    }

    public float calcularEnganche(float valorAuto, float porEnganche) {
        return valorAuto * (porEnganche / 100);
    }

    // Funcion calcultarPagoMensual
    public float calcularPagoMensual(float valorAuto, float porEnganche, int plazo) {
        float enganche = calcularEnganche(valorAuto, porEnganche);
        float totalFin = valorAuto - enganche;
        return totalFin/plazo;
    }

    // Cotizacion Terminada
}


